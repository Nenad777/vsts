SET IDENTITY_INSERT [dbo].[jobs] ON 

INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (1, N'Compensation or Benefits Specialist', 181, 236)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (2, N'Network Administrator', 39, 160)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (3, N'Revenue Agent', 51, 199)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (4, N'Financial Planner', 211, 45)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (5, N'Certified Public Accountant', 137, 24)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (6, N'Financial Specialist', 134, 182)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (7, N'Financial Examiner', 63, 95)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (8, N'Tax Preparer', 107, 164)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (9, N'Database Designer', 231, 227)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (10, N'Tax Examiner', 172, 179)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (11, N'Treasurer', 38, 164)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (12, N'Accountant', 149, 149)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (13, N'Budget Analyst', 117, 19)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (14, N'Business Analyst', 60, 229)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (15, N'Chief Financial Officer', 52, 138)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (16, N'Project Manager', 254, 14)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (17, N'Banking Analyst', 102, 59)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (18, N'Software Engineer', 59, 118)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (19, N'Auditor', 43, 7)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (20, N'Loan Counselor', 100, 29)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (21, N'Financial Analyst', 71, 127)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (22, N'Network Architect', 202, 90)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (23, N'Bookkeeper', 199, 208)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (24, N'Underwriter', 146, 162)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (25, N'Financial Manager', 192, 59)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (26, N'Business-to-Business Marketer', 235, 201)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (27, N'Mortgage Analyst', 94, 165)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (28, N'Assessor', 211, 55)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (29, N'Risk Analyst', 195, 73)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (30, N'Network Engineer', 203, 87)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (31, N'Network Installer', 81, 185)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (32, N'Cost Estimator', 37, 159)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (33, N'Loan Officer', 34, 137)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (34, N'Computer Operator', 81, 50)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (35, N'Database Administrator', 44, 182)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (36, N'Computer Programmer', 97, 42)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (37, N'Tax Collector', 111, 171)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (38, N'Banking Manager', 148, 239)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (39, N'Credit Analyst', 194, 125)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (40, N'Tax Manager', 109, 11)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (41, N'Accounting Manager', 63, 12)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (42, N'Personal Financial Advisor', 21, 185)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (43, N'Banker', 35, 218)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (44, N'System Administrator', 106, 43)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (45, N'Controller', 221, 59)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (46, N'Appraiser', 168, 241)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (47, N'Quality Assurance Specialist', 226, 39)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (48, N'Datawarehouse Designer', 185, 152)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (49, N'Mortgage Broker', 144, 91)
INSERT [dbo].[jobs] ([job_id], [job_desc], [min_lvl], [max_lvl]) VALUES (50, N'Help Desk Specialist', 250, 181)
SET IDENTITY_INSERT [dbo].[jobs] OFF
