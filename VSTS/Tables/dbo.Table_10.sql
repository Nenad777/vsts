SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Table_10] (
		[ID]     [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Table_10] SET (LOCK_ESCALATION = TABLE)
GO
